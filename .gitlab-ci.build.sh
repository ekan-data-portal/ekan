#!/bin/sh
docker compose exec -T php /bin/bash -c  "composer create-project drupal/recommended-project:^10 drupal"
docker compose exec -T php /bin/bash -c  "cd drupal && composer config --no-plugins allow-plugins.cweagans/composer-patches true"
docker compose exec -T php /bin/bash -c  "cd drupal && composer require cweagans/composer-patches"
docker compose exec -T php /bin/bash -c  "cd drupal && composer require drupal/coder"
docker compose exec -T php /bin/bash -c  "cd drupal && composer require symfony/phpunit-bridge"
docker compose exec -T php /bin/bash -c  "cd drupal && composer config minimum-stability dev"
docker compose exec -T php /bin/bash -c  "cd drupal && composer config extra.enable-patching --json true"
docker compose exec -T php /bin/bash -c  "cd drupal && composer require eightyoptions/ekan:2.x-dev"
docker compose exec -T php /bin/bash -c  "cd drupal && composer require weitzman/drupal-test-traits:^1"
docker compose exec -T php /bin/bash -c  "cd drupal && composer require drush/drush:^11" # see ekan-data-portal/ekan#64
# Add in pdf.js library for visualisation support
docker compose exec -T php /bin/bash -c  "mkdir drupal/web/libraries"
docker compose exec -T php /bin/bash -c  "cd drupal/web/libraries && wget https://github.com/mozilla/pdf.js/releases/download/v2.6.347/pdfjs-2.6.347-dist.zip -O pdf.js.zip"
docker compose exec -T php /bin/bash -c  "cd drupal/web/libraries && unzip pdf.js.zip -d pdf.js"
docker compose exec -T php /bin/bash -c  "cd drupal/web/libraries && rm pdf.js.zip"
