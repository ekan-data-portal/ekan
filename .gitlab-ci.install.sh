#!/bin/sh
# ensure the 'www-data' user can write to these folders
docker compose exec -T --user root php chown -R www-data:www-data drupal/web/sites
docker compose exec -T --user root php chown -R www-data:www-data docker-init
docker compose exec -T --user root php chmod -R 02775 drupal/web/sites/default/
docker compose exec -T --user root php chmod g+s drupal/web/sites/default/
docker compose exec -T --user root php mkdir /tmp/b2b
docker compose exec -T --user root php chown -R www-data:www-data /tmp/b2b
docker compose exec -T --user root php chmod -R 02775 /tmp/b2b
docker compose exec -T --user root php chmod g+s /tmp/b2b

# build the site
docker compose exec -T --user www-data php /bin/sh -c "cd drupal && while ! drush sql:query --db-url=mysql://drupal:drupal@mariadb:3306/drupal \"SHOW DATABASES;\"; do sleep 1; done"
docker compose exec -T --user www-data php /bin/sh -c "cd drupal && drush site:install ekan --db-url=mysql://drupal:drupal@mariadb:3306/drupal -y"
# Groups doesn't acknowledge user 1 power, we have to grant them a role.
docker compose exec -T --user www-data php /bin/sh -c "cd drupal && drush user:role:add --uid=1 administrator"