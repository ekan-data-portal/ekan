# Helper shell for running .gitlab-ci.yml locally
yml=`awk 'flag{ if (/after_script/){printf "%s", buf; flag=0; buf=""} else buf = buf $0 ORS}; /script/{flag=1}' .gitlab-ci.yml`
#Set the field separator to new line
IFS=$'\n'
for line in $yml
do
  line_trimmed=$(sed 's/^[ \t]*//;s/[ \t]*$//' <<< $line)
  #skip lines starts with #
  if  [[ $line_trimmed != \#* ]] ;
  then
    cmd=${line_trimmed:2}
    eval "$cmd"
  fi
done
