<?php

/**
* Settings for running the site in a local dev docker environment.
* Matches the docker config from docker-compose.yml in the project root
**/

$databases['default']['default'] = array(
  'driver' => getenv('DB_DRIVER'),
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USER'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST')
);

$databases['migrate']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'drupal',
  'password' => 'drupal',
  'host' => '192.168.1.52',
  'port' => 43333
);

// filesystem settings
$settings['file_private_path'] = '/var/www/html/docker-init/files/private';
// $settings['file_public_path'] = DRUPAL_ROOT . '/sites/default/files';
// $config['system.file']['path']['temporary'] = '/tmp';

// config settings
$settings['config_sync_directory'] = '../config/sync';
//$settings['config_sync_directory'] = 'profiles/contrib/ekan/config/install';

// Turn off debugging etc.
//$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
//$settings['cache']['bins']['discovery_migration'] = 'cache.backend.null';
//$settings['cache']['bins']['render'] = 'cache.backend.null';
//$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

// Turn on twig debugging. Configure twig.config debug, auto_reload and cache.
//$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/twig_debug.services.yml';
// Turn off SSL verify etc.
//$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/eopts.development.services.yml';

//$config['system.performance']['css']['preprocess'] = FALSE;
//$config['system.performance']['js']['preprocess'] = FALSE;

$settings['skip_permissions_hardening'] = TRUE;

// Only allow admins to create accounts by default.
$config['user.settings']['register'] = 'admin_only';
