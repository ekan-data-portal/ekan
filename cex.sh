#!/bin/sh
docker compose exec -T --user wodby php /bin/bash -c  "cd drupal && drush cex --destination=/var/www/html/drupal/web/profiles/contrib/ekan/config/install/ -y"
docker compose exec -T --user wodby php /bin/bash -c  "rm -f /var/www/html/drupal/web/profiles/contrib/ekan/config/install/core.extension.yml"
docker compose exec -T --user wodby php /bin/bash -c  "rm -f /var/www/html/drupal/web/profiles/contrib/ekan/config/install/update.settings.yml"
docker compose exec -T --user wodby php /bin/bash -c  "rm -f /var/www/html/drupal/web/profiles/contrib/ekan/config/install/migrate_drupal.settings.yml"
docker compose exec -T --user wodby php /bin/bash -c  "find /var/www/html/drupal/web/profiles/contrib/ekan/config/install/ -type f -exec sed -i -e '/^uuid: /d' {} \\;"
docker compose exec -T --user wodby php /bin/bash -c  "find /var/www/html/drupal/web/profiles/contrib/ekan/config/install/ -type f -exec sed -i -e '/_core:/,+1d' {} \\;"
