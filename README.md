# Installing EKAN

## The Short Version

```
  composer create-project eightyoptions/ekan_distribution:1.1.0 drupal
```

## Contributing to the EKAN profile repository

After cloning this project, make sure you have docker-compose installed and then run

1. docker-compose up -d
2. `sh .gitlab-ci.build.sh` (which runs the followings commands inside the php container).

```
composer create-project drupal/recommended-project:^9.5 drupal
cd drupal
composer config minimum-stability dev
composer config extra.enable-patching --json true
composer require eightyoptions/ekan:1.1.x-dev
```
3. Install drupal from http://ekan.localhost/core/install.php OR run the drush command `sh .gitlab-ci.install.sh`  
4. You should have a version of the ekan profile under `drupal/web/profiles/contrib/ekan`, including the reference to the remote git repository `https://gitlab.com/eightyoptions/ekan-profile.git`
5. NOTE: This is a different repository to the one hosted at drupalcode (`https://git.drupalcode.org/project/ekan/`), but the repositories should be synchronised when new releases are tagged.
6. If changes require config changes, there is a helper script in the root of this repository (`cex.sh`) which will perform a drush config export in to the ekan profile's install folder. This script will also strip out all the identifiers from the config yml files. `cex.sh` is intended to be run from the root of the project and is a list of `docker-compose` commands which operate on the php container.

## Developer setup steps
This allows you to work on a git managed version of the ekan profile while
it is running in this project.

1. Clone the profile repository a folder adjacent to this one.  
`git clone git@gitlab.com:eightyoptions/ekan-profile.git ./../ekan_profile`

2. Replace the composer required version (from above) with a relative path symlink  
 `cd drupal/web/profiles/contrib`  
 `rm -fr ekan`  
 `ln -s ../../../../../ekan_profile ekan`  

## The Longer Version - A Local Drupal Development Environment

### Docker & Docker-compose

Docker (https://www.docker.com/) is a popular container management platform. Using the docker-compose tool we can define and run multi-container systems in a repeatable way. Docker-compose allows us to build up a recipe for an environment, this recipe can be stored under version control alongside the project code. 

The EKAN codebase contains a docker-compose.yml file in the project root, this file defines a software stack that can be used to spin up the project for local development. 

In order to use the provided development environment you will need to ensure that both Docker and Docker-compose are installed on you local system.

https://docs.docker.com/get-docker/

https://docs.docker.com/compose/install/

The following software stack is defined in the EKAN docker-compose file :

### Mariadb

 Mariadb is the database engine used by the Drupal project to store data.

“MariaDB is a community-developed fork of the MySQL relational database management system intended to remain free under the GNU GPL..”

### PHP 8

PHP is the scripting language in which Drupal is written. The  PHP container contains the PHP 8 installation and additionally contains the following tools which are critical to the Drupal development workflow: 

Drush: Used to perform many powerful Drupal maintenance tasks from the command line “Drush is a command line shell and Unix scripting interface for Drupal.”

Composer: Used to manage the Drupal installation including dependencies, contributed modules and patches, “Composer is a tool for dependency management in PHP.”

### Nginx

Nginx is used as a webserver, serving the Drupal web application to an open port on the host. This enables the site to a visible by a web browser locally on a development environment or publicly in a production environment.

“Nginx (pronounced "engine-x") is an open source reverse proxy server for HTTP, HTTPS, SMTP, POP3, and IMAP protocols, as well as a load balancer, HTTP cache, and a web server (origin server).”

### Mailhog

MailHog is an email testing tool for developers, in our development environment it is used to catch and inspect outgoing mail. 

### Node

The EKAN development environment contains a node container running a SASS compiler. This container watches the drupal/web/themes/custom/ekan_theme/ folder for changes to SASS files, once a change is detected it automatically compiles the SASS into CSS making theming changes instantly visible.

### Traefik

Traefik is an open-source Edge Router, in our development environment it routes local traffic from human readable URLs to the various containers.  In this case the PROJECT_BASE_URL is set to ekan.localhost, requests to this URL will be routed to the NGINX webserver and present you with your development site in the browser.

### Composer

As is standard practice for all Drupal 8 projects, the installation of Drupal and it’s dependencies is managed using Composer, 

IMPORTANT NOTE: we will be using docker-compose to manage our software stack and composer to manage Drupal PHP dependencies, despite the similarity in the names THESE ARE COMPLETELY SEPARATE APPLICATIONS.

‘Composer is a dependency manager for PHP. Drupal core uses Composer to manage core dependencies’.

Composer assists us to locate, download, validate, and load Drupal packages. We will use composer to update Drupal Core, we will also use composer to manage contributed modules, themes and patches.

Composer comes pre-packaged in the PHP container provided with the EKAN development environment, there is no need to install separately.


# Basic development tasks

In this section we will cover some basic development tasks. We are assuming that you have started your environment with docker-compose up -d, all commands are assumed to be run from the project root.

Start the development environment

Here we run docker-compose which looks at the docker-compose.yml file in the project root for a recipe to create the environment. Ensure this command is run from the root of your project folder.

```docker-compose up -d```

You should now be able to use your web browser to access the following local services:

http://ekan.localhost - Gives you access to your Drupal installation, browsing to this URL should show you the default Drupal install screen.

http://mailhog.ekan.localhost - The mailhog service catches emails sent from the development environment.


# Using an existing database backup

Often as a developer you will want to import an existing database into your development environment. This can be achieved by placing your .sql dump file into a folder named mariadb-init in the project root. 

You will want to perform this step before starting your environment with the docker-compose up -d command.

The mariadb image looks in this location for a *.sql file, if one exists it will load that database backup automatically.

You can also load a database backup manually with the following commands.

```docker-compose exec php sh # Enters the php container.
cd /var/www/html/drupal/ # Enters the Drupal root path.
drush sql-drop # Truncates any existing database tables.
drush sql-cli < ../docker-init/db/[backup-file-name].sql # Loads the database backup.
```

Once complete you will be able to browse to http://ekan.localhost and see an installation of the EKAN project. This method builds the EKAN project including all content captured at the time the database backup was taken.

Stopping the development environment

In order to stop the development environment we perform the following. Ensure this command is run from the root of your project folder:

```docker-compose stop```

If we wish to completely stop and reset the development environment discarding the database(!) we can use the following command:

```docker-compose down -v```

Execute commands inside a container

There are two ways that we can execute a command inside a running container:

Execute a single command using docker-compose exec: 

The docker-compose exec command is used to execute a single command inside a container, in the following example we will execute the ls command inside the php container :

```docker-compose exec php ls```

The result will be a file listing of the inside of the php container.

Access the container via an interactive prompt:

Accessing the container via a shell is the equivalent of ‘logging in’ to the container. You are free to explore the file system and run commands. Use the following command to access a shell in the php container:

```docker-compose exec php sh```

# Using Composer to manage Drupal Dependencies

As above we can now run composer commands in our PHP container using the docker-compose exec php sh method (remember docker-compose and composer are completely separate applications despite the naming similarity). 

Composer looks in the drupal/composer.json file to see which dependencies, modules and themes have been specified for the Drupal installation. 

# Updating Drupal Core

PLEASE NOTE: The below example shows a very simplistic upgrade process. More considerations are often required, please see https://www.drupal.org/docs/updating-drupal/updating-drupal-core-via-composer for more detailed upgrade instructions.

We will use composer here to update Drupal core to the latest stable version. We will use docker-compose to run composer inside the docker PHP container.

```docker-compose exec php sh```

we are now operating inside the PHP container

```
cd drupal
composer update drupal/core
exit
```

Let’s break this command down in order to understand what is going on, docker-compose exec php sh is telling the system to login to the PHP container.

 ```composer update drupal/core``` is the command to be run inside the php container, in this case we are using composer to upgrade drupal core to the latest version.

```exit``` lets us leave the php container shell environment.

## Enable docker based sass compiler
1. Uncomment the "node" section in the docker-compose.yml file
2. replace the `working_dir:` value with the path to your newly created custom theme
3. restart the docker containers. `docker-compose up`

You can monitor the sass compiler logs with the following command:
`docker-compose logs -f node`