#!/bin/sh
# run tests
docker compose exec -T --user root php chown -R www-data:www-data drupal/web/sites
docker compose exec -T --user root php chmod -R 02775 drupal/web/sites/default/files
docker compose exec -T --user root php chmod g+s drupal/web/sites/default/files
docker compose exec -T --user www-data php /bin/sh -c "cd drupal && ./vendor/bin/phpunit --configuration=../phpunit.xml"